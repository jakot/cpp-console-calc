/**************************************************************************************************
 * 
 *      Simple console calculator made as an excercise
 *      to practice OOP in C++.
 *
 *      Last modification: 2019-04-22
 *      Bugs not found.
 *
 *      This programme has been written as an excercise and
 *      can be refactored and improved in many ways.
 *
 *      To improve readability some functions can be encapsulated.
 *      Using hashtables or tree maps would make programme running faster.
 *      More mathematical functions and better syntax of already implemented ones
 *      can also improve usage.
 *      Feel free to use this code wherever you need.
 *      You can contact me on jakubkot0@o2.pl
 * 
 *************************************************************************************************/

#include <iostream>
#include <vector>
#include <string>
#include <math.h>
#include <stdexcept>

using namespace std;

// All required constants =========================================================================

// Names of token kinds
// Implementation specific
const char number   = 'n';      // numbers
const char name     = 'a';      // names
const char decl     = 'l';      // symbol declare
const char help_c   = 'h';      // help
const char sqrt_c   = 's';      // square root
const char pow_c    = 'y';      // power
const char print    = '\n';     // result print
const char quit     = 'q';      // calc quit
const char display  = 'd';      // symbol table display
const char clscreen = 'c';      // clear screen

// Keywords to type while running calc
const string decl_key       = "let";        // symbol declaration
const string help_key       = "help";       // help display
const string calcsqrt_key   = "sqrt";       // square root calculation
const string calcpow_key    = "pow";        // power calculation
const string quit_key       = "quit";       // calc quit
const string display_key    = "symbols";    // symbol table display
const string cls_key        = "clear";      // clear screen

// Prompt & res
const string prompt         = "> ";
const string result         = "= ";

/// Help.
/// Can be modified in order to change language and/or add some more information.
void help() {
    cout << "\n\n"
         << "\t\tCalc help:\n"
         << "\tProgramme follows sequence of operations,\n"
         << "\tYou can write these operators in order to make calculations:\n"
         << "\t'+', '-', '*', '/', '%' (modulo operator - works only with integers),\n"
         << "\t! - as a suffix to calculate factorial,\n"
         << "\tsqrt('expression') - square root of expression and pow('base', 'exponent') - power\n"
         << "\twhere exponent must be an integer.\n"
         << "\tYou can use parentheses and brackets (except from 'sqrt' and 'pow') -\n"
         << "\t- only '(' and ')' there.\n"
         << "\tTo calculate typed sequence just press enter.\n"
         << "\tYou can assign specific values to named variables\n"
         << "\tusing formula: '" << decl_key << " variable_name = expression'.\n"
         << "\tTo change existing variable's value type: 'variable_name = new_value'\n"
         << "\tTo display all declared symbols use: '" << display_key <<"'.\n"
         << "\tTo clear console type: '" << cls_key << "'.\n"
         << "\tTo quit programme use: '" << quit_key << "'.\n"
         << "\tTo display this help: '" << help_key << "'.\n"
         << "\n\n";
}

// Basic classes ==================================================================================

/// Fundamental structure, single token representing a piece of expression.
/// Has fixed value if represents a number or a name.
class Token {
    public:
        Token () : kind() {}
        Token (char ch) : kind(ch) {}
        Token (char ch, double val) : kind(ch), value(val) {}
        Token (char ch, string s) : kind(ch), name(s) {}

        char kind;
        double value;
        string name;
};

/// Makes evaluation simplier, uses buffer in order to let being used as a stream.
class Token_stream {
    public:
        Token_stream() : full(false), buffer(0) {}
        Token get();
        void ignore(char c);
        void unget();
    private:            // Stream buffer
        bool full;
        Token buffer;
};

/// A symbol with fixed value.
class Variable {
    public:
        string name;
        double val;
        Variable (string n, double value) : name(n), val(value) {}
};

/// Contains all declared variables and makes adding, deleting, changing and displaying them possible.
class Symbol_table {
    public:
        Symbol_table() : var_table() {}

        bool is_declared(string s);
        double get(string s);
        void set(string s, double v);
        void declare(Token_stream& ts);
        void display();

    private:
        vector<Variable> var_table;
};

// Global variables declarations:

Token_stream ts;
Symbol_table st;

// Token_stream and symbol table methods definitions ===============================================

/// Puts last taken token back into stream's buffer.
void Token_stream::unget() {
    if (full) throw runtime_error("Buffor overload.");

    full = true;
}

// Gets single token from stream using buffer and standard character input.
Token Token_stream::get() {
    if (full) {
        full = false;
        return buffer;
    }

    char c = cin.get();

    while (isspace(c) && c != print)    // Skipping whitespaces.
        c = cin.get();

    Token res;
    switch (c){
        case '(': case ')':
        case '{': case '}':
        case '+': case '-':
        case '*': case '/':
        case '%': case '=':
        case '!': case ',':
        case print:
            res = Token(c);
            break;
        default:
            if (isdigit(c)){    // Loading a number.
                cin.unget();
                double d;
                cin >> d;
                res = Token(number, d);
            } else if (isalpha(c)){    // Loading a word (variable name or a keyword).
                string s;
                s += c;

                while (cin.get(c) && (isalpha(c) || isdigit(c) || c=='_'))
                    s += c;
                cin.unget();
                
                // Dealing with keywords and variables
                if (s == decl_key)          res = Token(decl);
                else if (s == quit_key)     res = Token(quit);
                else if (s == calcpow_key)  res = Token(pow_c);
                else if (s == calcsqrt_key) res = Token(sqrt_c);
                else if (s == help_key)     res = Token(help_c);
                else if (s == display_key)  res = Token(display);
                else if (s == cls_key)      res = Token(clscreen);
                else res = Token(name, s);
            } else throw runtime_error("Wrong token.");
        }
    buffer = res;
    return res;
}

// Clears useless characters in stream.
void Token_stream::ignore(char c) {
    if (full && c == buffer.kind){
        full = false;
        return;
    }

    full = false;
    char ch;
    while (ch = cin.get())
        if (ch == c)
            return;
}

double expression(Token_stream&);   // Forward declaration.


// Checks whether 's' is declared in table.
bool Symbol_table::is_declared(string s) {
    for (int i = 0; i < var_table.size(); i++)
        if (var_table[i].name == s)
            return true;

    return false;
}

// Gets symbol's value.
double Symbol_table::get(string s) {
    for (int i = 0; i < var_table.size(); i++)
        if (var_table[i].name == s)
            return var_table[i].val;

    throw runtime_error("Undeclared symbol: " + s);
}

// Sets new variable's value or changes existing one.
void Symbol_table::set(string s, double v) {
    for (int i = 0; i < var_table.size(); i++)
        if (var_table[i].name == s){
            var_table[i].val = v;
            return;
        }
    throw runtime_error("Attempt to set value of undeclared symbol: " + s);
}

// Declares new variable and sets it's value.
void Symbol_table::declare(Token_stream& ts) {
    Token t = ts.get();
    if (t.kind != name)
        throw runtime_error("Symbol's name expected in declaration.");

    if (is_declared(t.name))
        throw runtime_error("Attempt to declare already declared symbol: " + t.name);
    
    Token t2 = ts.get();
    if (t2.kind != '=')
        throw runtime_error("expected '=' in declaration.");

    var_table.push_back(Variable(t.name, expression(ts)));
}

// Displays all declared variables and their values.
void Symbol_table::display() {
    cout << "Symbol table:" << endl;
    for (int i = 0; i < var_table.size(); i++)
        cout << var_table[i].name << " == " << st.get(var_table[i].name) << endl;
}

//=================================================================================================

void cls() {
    system("clear");
}

// Cleans token stream. Used after exceptions.
void clean_up_mess(Token_stream& ts) {
    ts.ignore(print);
}


// Grammar implementation =========================================================================

// Using standard grammar of basic mathematical expressions.
// Evaluating expressions with signs: '+', '-', '*', '/', '%', '!'
// and keywords calling functions to calculate power and square root

// Operates on brackets, single numbers and 'pow' and 'sqrt' funcions.
double primary(Token_stream& ts) {
    Token t = ts.get();
    switch (t.kind) {
        case number:
            return t.value;
        case ')': case '}': case ']':
            throw runtime_error("Lack of opening bracket.");
        
        case '(': {
            double d = expression(ts);
            t = ts.get();
            if (t.kind != ')') {ts.unget(); throw runtime_error("Expected ')' token.");}
            return d;
        }
        case '{': {
            double d = expression(ts);
            t = ts.get();
            if (t.kind != '}') {ts.unget(); throw runtime_error("Expected '}' token.");}
            return d;
        }
        case '[': {
            double d = expression(ts);
            t = ts.get();
            if (t.kind != ']') {ts.unget(); throw runtime_error("Expected ']' token.");}
            return d;
        }

        case '+':
            return primary(ts);
        case '-':
            return -primary(ts);
        
        case sqrt_c: {
            t = ts.get();
            if (t.kind != '(') {ts.unget(); throw runtime_error("Expected '(' token after 'sqrt'.");}
            double d = expression(ts);
            t = ts.get();
            if (t.kind != ')') {ts.unget(); throw runtime_error("Expected ')' token after expression.");}
            return sqrt(d);
        }
        case pow_c: {
            t = ts.get();
            if (t.kind != '(') {ts.unget(); throw runtime_error("Expected '(' token after 'pow'.");}
            double left = expression(ts);
            t = ts.get();
            if (t.kind != ',') {ts.unget(); throw runtime_error("Expected ',' token after number to power.");}
            double right = expression(ts);
            t = ts.get();
            if (t.kind != ')') {ts.unget(); throw runtime_error("Expected ')' token after exponent.");}
            if (right < 0) {
                left = 1/left;
                right *= -1;
            }
            int i(right);
            if (i != right) throw runtime_error("Exponent must be an integer.");
            double res = 1;
            for (int j = 1; j <= i; j++) res *= left;
            return res;
        }

        case name:
            return st.get(t.name);
        default:
            ts.unget();
            throw runtime_error("Expected number or symbol token.");
    }
}

// Auxiliary function to enable factorial calculation.
double primary2(Token_stream& ts) {
    double left = primary(ts);
    Token t = ts.get();
    if (t.kind != '!'){
        ts.unget();
        return left;
    }
    int i(left);
    if (i != left || i < 0) throw runtime_error("The number to calculate factorial must be natural.");
    if (i == 0) return 1;
    for (int j = i - 1; j > 1; j--) left *= j;
    return left;
}

// Evaluates parts separated with '*', '/' and '%' operators.
double term(Token_stream& ts) {
    double left = primary2(ts);
    while (true){
        Token t = ts.get();
        switch (t.kind){
        case '*':
            left *= primary2(ts);
            break;
        case '/':{
            double d = primary2(ts);
            if (d == 0) throw runtime_error("Dividing by 0.");
            left /= d;
            break;
        }
        case '%':{
            double right = primary2(ts);
            int l(left), r(right);
            if (l != left || r != right) throw runtime_error("Expected only integers to calculate modulo.");
            return l %= r;
        }
        default:
            ts.unget();
            return left;
        }
    }
}

// Evaluates expressions containing '+' and '-' operators.
double expression(Token_stream& ts){
    double left = term(ts);
    while (true){
        Token t = ts.get();
        switch (t.kind){
        case '+':
            left += term(ts);
            break;
        case '-':
            left -= term(ts);
            break;
        default:
            ts.unget();
            return left;
        }
    }
}

// Calculates expressions, declares new symbols and makes assignments to already declared ones.
double statement(Token_stream& ts){
    Token t = ts.get();
    switch (t.kind){
        case  decl: {
            Token t2 = ts.get();
            ts.unget();
            st.declare(ts);
            return st.get(t2.name);
        }
        case name: {
            if (!st.is_declared(t.name))
                throw runtime_error("Undeclared symbol: " + t.name);

            char c; // Using char instead of Token because of impossibility to evaluate expression
                    // when not redeclaring symbol.
            while (c = cin.get())   // Skipping whitespaces between symbol name and next token
                if (c != ' ')
                    break;

            if (c == '=') {
                double right = expression(ts);
                st.set(t.name, right);
                return st.get(t.name);
            }
            cin.putback(c);
        }
        default:
            ts.unget();
            return expression(ts);
    }
}

// Main function evaluating every typed sequence;
// Operates on quit, help, display and cls keywords; passes rest to statement(Token_stream&);
void calculation(Token_stream& ts){
    cout << "Welcome to my simple console calculator, to display help just type '" << help_key << "'.\n";
        Token t;
        while (true) try{
            do {
                cout << prompt;
                t = ts.get();
            } while (t.kind == print);

            switch (t.kind) {
                case quit: return;
                case help_c: help(); break;
                case display: st.display(); break;
                case clscreen: cls(); break;
                default:
                    ts.unget();
                    cout << result << statement(ts) << endl;
            }
            t = ts.get();   // To avoid double prompt after every statement evaluation.
            if (t.kind != print) throw runtime_error("Expected print token at the end of statement.");
        }
        catch (runtime_error& e){
            cerr << e.what() << endl;
            clean_up_mess(ts);
        }
}

int main()

    try {
        calculation(ts);
        return 0;
    }

    catch (exception& e) {  // Catching exceptions missed in calculation();
            cerr << "Exception: " << e.what() << endl;
            clean_up_mess(ts);
            return 1;
    }

    catch (...) {
            cerr << "Unexpected exception.\n";
            clean_up_mess(ts);
            return 1;
    }
